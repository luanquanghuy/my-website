const express = require("express");
const loginRouter = require("./login");
const signUpRouter = require("./signup");

const app = new express();

app.use("/login", loginRouter);

app.use("/signup", signUpRouter);

app.get("/", ((req, res) => {
    res.send("Home Page");
}))

app.get("/hot-fix", ((req, res) => {
    res.send("Hot fix");
}))

app.listen(3000, () => {
    console.log("Server is running port 3000")
})
